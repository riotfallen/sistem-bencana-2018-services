<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function get_kota(){
    if (isset($_GET["prov"])){
        $provId = $_GET["prov"];

        $conn = connection();
        $query = "SELECT RKOTA_ID, RKOTA_NAMA FROM R_KOTA WHERE RPROV_ID = $provId";
        $sqlResult = mysqli_query($conn, $query);
        
        $data = array();
        
        if(mysqli_num_rows($sqlResult) == 0 ){
            header("HTTP/1.1 404 Not Found");
            $data = null;
        } else {
            while($row = mysqli_fetch_array($sqlResult)){
                $kotaId = $row["RKOTA_ID"];
                $kotaName = $row["RKOTA_NAMA"];
                $data[] = array(
                    'id' => $kotaId,
                    'nama' => $kotaName,
                );
            }
        }

        if($data != null){
            $result = array(
                "error" => false,
                "messege" => "ok",
                "kota" => $data
            );
        } else {
            $result = array(
                "error" => true,
                "messege" => "Not Found",
                "kota" => $data
            );
        }

        return $result;
    } else {
        header("HTTP/1.1 400 Bad Request");
        return $data = array(
            "error" => true,
            "messege" => "Parameter not set",
            "kota" => null
        );
    }
	
}
echo json_encode(get_kota());
?>

