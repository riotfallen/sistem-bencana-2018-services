<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function do_login(){
    $data = array();
    if(isset($_POST["email"]) && isset($_POST["password"])){
        $email = $_POST["email"];
        $password = $_POST["password"];
        $user = get_user_data($email);
        if($user != null){
            $hashPassword = $user['password'];
            if(password_verify($password, $hashPassword)){
                $user = get_user_data($email);
                $token = update_token_login($user['userId']);
                $data = array(
                    'error' => false,
                    'messege' => 'Granted',
                    'auth' => array(
                        'userId' => $user['userId'],
                        'firstName' => $user['firstName'],
                        'lastName' => $user['lastName'],
                        'email' => $user['email'],
                        'avatar' => $user['avatar'],
                        'token' => $token
                    )
                );
            } else {
                header("http/1.1 401 Unauthorized");
                $data = array(
                    'error' => true,
                    'messege' => 'Denied',
                    'auth' => null
                );
            }
        } else {
            header("http/1.1 404 Not Found");
            $data = array(
                'error' => true,
                'messege' => 'User not Found',
                'auth' => null
            );
        }
    } else {
        header("http/1.1 400 Bad Request");
        $data = array(
            'error' => true,
            'messege' => 'Parameter not set',
            'auth' => null
        );
    }
    return $data;
}

function get_user_data($email){
    $conn = connection();
    $query = "SELECT u.SYSUSER_ID, u.SYSUSER_NAMADEPAN, u.SYSUSER_NAMABELAKANG, u.SYSUSER_EMAIL, u.SYSUSER_PASSW, u.SYSUSER_AVATAR 
    FROM SYS_USER u 
    WHERE u.SYSUSER_EMAIL = '$email'";

    $sqlResult = mysqli_query($conn, $query);
    if(mysqli_num_rows($sqlResult) != 0){
        while($row = mysqli_fetch_array($sqlResult)){
            $user = array(
                "userId" => $row['SYSUSER_ID'],
                "firstName" => $row['SYSUSER_NAMADEPAN'],
                "lastName" => $row['SYSUSER_NAMABELAKANG'],
                "email" => $row['SYSUSER_EMAIL'],
                "password" => $row['SYSUSER_PASSW'],
                "avatar" => $row['SYSUSER_AVATAR']
            );
        }
    } else {
        return null;
    }
    return $user;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function update_token_login($userId){
    $conn = connection();
    $ranToken = generateRandomString(32);
    $query = "UPDATE `bencana2018`.`SYS_USER` SET `SYSUSER_TOKENLOGIN` = '$ranToken' WHERE (`SYSUSER_ID` = '$userId')";

    if(mysqli_query($conn, $query) === TRUE){
        return $ranToken;
    } else {
        return "";
    }
}

echo json_encode(do_login());
?>