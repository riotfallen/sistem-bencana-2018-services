<?php 
require 'connection.php';
function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function send_notification($tokens, $title, $body){
    $url = 'https://fcm.googleapis.com/fcm/send';
    $priority="high";
    $notification= array('title' => $title,'body' => $body );

    $fields = array(
         'registration_ids' => $tokens,
         'notification' => $notification
        );

    $headers = array(
        'Authorization:key=AIzaSyAmSn1YGspO-7Jjqw4h04ggTtyionEQcjY',
        'Content-Type: application/json'
        );

   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
   $result = curl_exec($ch);           
   echo curl_error($ch);
   if ($result === FALSE) {
       die('Curl failed: ' . curl_error($ch));
   }
   curl_close($ch);
   return $result;
}

function push(){
    $conn = connection();
	$sql = "SELECT RTOKEN_DATA FROM R_TOKEN";
	$result = mysqli_query($conn,$sql);
	$tokens = array();
	if(mysqli_num_rows($result) > 0 ){

		while ($row = mysqli_fetch_assoc($result)) {
			$tokens[] = $row["RTOKEN_DATA"];
		}
	}
	mysqli_close($conn);
    $body = $_GET['body'];
    $title = $_GET['title'];
	$message_status = send_notification($tokens, $title, $body);
	return $message_status;
}
echo push();
 ?>