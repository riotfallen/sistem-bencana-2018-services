<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function get_kec(){
    if (isset($_GET["kota"])){
        $kotaId = $_GET["kota"];

        $conn = connection();
        $query = "SELECT RKEC_ID, RKEC_NAMA FROM R_KEC WHERE RKOTA_ID = $kotaId";
        $sqlResult = mysqli_query($conn, $query);
        
        $data = array();
        
        if(mysqli_num_rows($sqlResult) == 0 ){
            header("http/1.1 404 Not Found");
            $data = null;
        } else {
            while($row = mysqli_fetch_array($sqlResult)){
                $kecId = $row["RKEC_ID"];
                $kecName = $row["RKEC_NAMA"];
                $data[] = array(
                    'id' => $kecId,
                    'nama' => $kecName,
                );
            }
        }

        if($data != null){
            $result = array(
                "error" => false,
                "messege" => "ok",
                "kecamatan" => $data
            );
        } else {
            $result = array(
                "error" => true,
                "messege" => "Not Found",
                "kecamatan" => $data
            );
        }

        return $result;
    } else {
        header("http/1.1 400 Bad Request");
        return $data = array(
            "error" => true,
            "messege" => "Parameter not set",
            "kecamatan" => null
        );
    }
	
}
echo json_encode(get_kec());
?>

