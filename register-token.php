<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function do_register_token(){
    if(isset($_GET["token"])){
        $token = $_GET["token"];
        $conn = connection();
        $data = array();

        $query = "INSERT INTO R_TOKEN VALUES(null, '$token')";

        if(mysqli_query($conn, $query) == FALSE){
            $data = array(
                'error' => true,
                'messege' => 'Failed to insert token',
                'token' => null
            );
        } else {
            $data = array(
                'error' => false,
                'messege' => 'Token Affected',
                'token' => $token
            );
        }
    } else {
        $data = array(
            'error' => true,
            'messege' => 'Failed to insert token',
            'token' => null
        );
    }

    return $data;
}

echo json_encode(do_register_token());
?>