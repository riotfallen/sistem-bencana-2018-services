<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function do_regist(){
    if(isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["email"]) && isset($_POST["password"])
    && isset($_POST["alamat"]) && isset($_POST["kelurahan"]) && isset($_POST["url"])){
        $firstName = strtoupper($_POST["firstName"]);
        $lastName = strtoupper($_POST["lastName"]);
        $email = $_POST["email"];
        $password = $_POST["password"];
        $alamat = $_POST["alamat"];
        $kelurahan = $_POST["kelurahan"];
        $url = $_POST["url"];

        if(!email_exist($email)){
            $conn = connection();
            $encryptedPas = encryptIt($password);
            $token = generateRandomString(32);
            $query = "INSERT INTO SYS_USER VALUES(null, 1, '$firstName', '$lastName', '$email', '$encryptedPas', '$alamat', $kelurahan, '$url', TRUE, 0,null,CURDATE(), '$token')";
        

            if(!mysqli_query($conn, $query)){
                header("http/1.1 400 Bad Request");
                return $data = array(
                    "error" => true,
                    "messege" => mysqli_error($conn),
                    "auth" => null
                );
            } else {
                $userId = get_user_id($email);
                return $data = array(
                    "error" => false,
                    "messege" => "Register Successfully",
                    "auth" => array(
                        "userId" => $userId,
                        "fistName" => $firstName,
                        "lastName" => $lastName,
                        "email" => $email,
                        "avatar" => $avatar,
                        "token" => $token
                    )
                );
            }
        } else {
            header("http/1.1 400 Bad Request");
            return $data = array(
                "error" => true,
                "messege" => "Email already registered, please login",
                "auth" => null
            );
        }

        
    } else {
        header("http/1.1 400 Bad Request");
        return $data = array(
            "error" => true,
            "messege" => "Parameter not set",
            "auth" => null
        );
    }
}

function get_user_id($email){
    $conn = connection();
    $query = "SELECT SYSUSER_ID FROM SYS_USER WHERE SYSUSER_EMAIL = '$email'";

    $sqlResult = mysqli_query($conn, $query);
    if(mysqli_num_rows($sqlResult) == 0){
        return null;
    } else {
        $row = mysqli_fetch_assoc($sqlResult);
            return $row["SYSUSER_ID"];
    }
}

function email_exist($email){
    $conn = connection();
    $query = "SELECT SYSUSER_ID FROM SYS_USER WHERE SYSUSER_EMAIL = '$email'";

    $sqlResult = mysqli_query($conn, $query);
    if(mysqli_num_rows($sqlResult) == 0){
        return false;
    } else {
        return true;
    }
}

function encryptIt( $q ) {
    return password_hash($q, PASSWORD_DEFAULT);
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

echo json_encode(do_regist());
?>