<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function get_kel(){
    if (isset($_GET["kec"])){
        $kecId = $_GET["kec"];

        $conn = connection();
        $query = "SELECT RKEL_ID, RKEL_NAMA FROM R_KEL WHERE RKEC_ID = $kecId";
        $sqlResult = mysqli_query($conn, $query);
        
        $data = array();
        
        if(mysqli_num_rows($sqlResult) == 0 ){
            header("http/1.1 404 Not Found");
            $data = null;
        } else {
            while($row = mysqli_fetch_array($sqlResult)){
                $kelId = $row["RKEL_ID"];
                $kelName = $row["RKEL_NAMA"];
                $data[] = array(
                    'id' => $kelId,
                    'nama' => $kelName,
                );
            }
        }

        if($data != null){
            $result = array(
                "error" => false,
                "messege" => "ok",
                "kelurahan" => $data
            );
        } else {
            $result = array(
                "error" => true,
                "messege" => "Not Found",
                "kelurahan" => $data
            );
        }

        return $result;
    } else {
        header("http/1.1 400 Bad Request");
        return $data = array(
            "error" => true,
            "messege" => "Parameter not set",
            "kelurahan" => null
        );
    }
	
}
echo json_encode(get_kel());
?>

