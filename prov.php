<?php
require 'connection.php';
header('Content-Type: application/json');

function connection(){
	global $servername;
	global $username;
	global $password;
	global $dbname;

	return mysqli_connect($servername,$username,$password,$dbname);
}

function get_prov(){
	$conn = connection();
	$query = "SELECT RPROV_ID, RPROV_NAMA FROM R_PROV";
	$sqlResult = mysqli_query($conn, $query);
	
	$data = array();
    
    if(mysqli_num_rows($sqlResult) == 0 ){
        header("HTTP/1.1 404 Not Found");
        $data = null;
    } else {
        while($row = mysqli_fetch_array($sqlResult)){
            $provId = $row["RPROV_ID"];
            $provName = $row["RPROV_NAMA"];
            $data[] = array(
                'id' => $provId,
                'nama' => $provName,
            );
        }
    }

    if($data != null){
        $result = array(
            "error" => false,
            "messege" => "ok",
            "provinsi" => $data
        );
    } else {
        $result = array(
            "error" => true,
            "messege" => "Not Found",
            "provinsi" => $data
        );
    }

    return $result;
}


echo json_encode(get_prov());
?>

